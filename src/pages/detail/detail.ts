import { Component } from '@angular/core';
import { BarcodeScanner } from 'ionic-native';
import { NavController,NavParams } from 'ionic-angular';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import {Http} from "@angular/http";

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html'
})
export class DetailPage {
item:any;
ad:any;
soyad:any;
img:any;
detail:any;
back:any;


  constructor(public navCtrl: NavController,public http:Http,public params:NavParams) {
    this.item=params.get('item');
    this.back=params.get('back');
    this.ad=localStorage.getItem("ad");
    this.soyad=localStorage.getItem("soyad");
    this.img=localStorage.getItem("img");
    this.detail={};

    let link="http://www.abctasarim.net/deneme/detail.asp?id="+this.item;
    this.http.get(link).map(res=>res.json()).subscribe(data=>{
      this.detail=data.data[0];
    });
  }


}
