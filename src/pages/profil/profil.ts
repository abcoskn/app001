import { Component } from '@angular/core';
import { BarcodeScanner } from 'ionic-native';
import { NavController,AlertController } from 'ionic-angular';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import {Http} from "@angular/http";
import { DetailPage } from '../detail/detail';
import { FabContainer } from 'ionic-angular';

@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html'
})
export class ProfilPage {
barkod:any;
bilgi:any;
items:any;
ad:any;
soyad:any;
img:any;

  constructor(public navCtrl: NavController,public http:Http,public alertCtrl:AlertController) {
    this.bilgi={};
    this.ad=localStorage.getItem("ad");
    this.soyad=localStorage.getItem("soyad");
    this.img=localStorage.getItem("img");
    let link="http://www.abctasarim.net/deneme/cek.asp";
    this.http.get(link).map(res=>res.json()).subscribe(data=>{
      this.items=data.data;
    });
let link2="http://www.abctasarim.net/deneme/bilgi.asp?id="+localStorage.getItem("id");
    this.http.get(link2).map(res=>res.json()).subscribe(data2=>{
      this.bilgi=data2.data[0];
    });
  }
  tara()
  {
        BarcodeScanner.scan({
            preferFrontCamera: false,
            showFlipCameraButton: true,
        }).then((barcodeData) => {
        let alert = this.alertCtrl.create({
          title: 'Kod içeriği',
          subTitle: barcodeData.text,
          buttons: ['OK']
        });
        alert.present();
}, (err) => {
    // An error occurred
});
  }
  goDetail(item,back)
  {
    this.navCtrl.push(DetailPage,{
      item:item,
      back:back
    });
  }
  refresh(fab: FabContainer){
      fab.close();
}

}
