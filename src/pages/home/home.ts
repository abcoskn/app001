import { Component } from '@angular/core';
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import { NavController,AlertController  } from 'ionic-angular';
import { ProfilPage } from '../profil/profil';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
telefon:any;
sonuc:any;
  constructor(public navCtrl: NavController,public alertCtrl: AlertController,public http:Http) {
    this.telefon="";
    this.sonuc="";
  }
  login()
  {
    let link="http://www.abctasarim.net/deneme/login.asp?telefon="+this.telefon;
    this.http.get(link).map(res=>res.text()).subscribe(data=>{

      if(data=="false")
      {
        let alert = this.alertCtrl.create({
          title: 'Hata!',
          subTitle: 'Telefon numarasını hatalı girdiniz. Lütfen tekrar deneyin.',
          buttons: ['OK']
        });
        alert.present();
      }
      else
      {
      this.giris(data);    
      }
    });

  }
  giris(dt){

        this.http.get("http://www.abctasarim.net/deneme/bilgi.asp?id="+dt).map(res=>res.json()).subscribe(data2=>{
          localStorage.setItem("id",data2.data[0].id);
          localStorage.setItem("ad",data2.data[0].ad);
          localStorage.setItem("soyad",data2.data[0].soyad);
          localStorage.setItem("img",data2.data[0].img);
        });
        this.navCtrl.setRoot(ProfilPage);
  }

}
